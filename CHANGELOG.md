# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.5](https://gitlab.com/freedomsex/components/entity-date-update/compare/0.1.4...0.1.5) (2022-11-23)


### Bug Fixes

* skip Overwrite Initial value in updated fields ([ef2e52e](https://gitlab.com/freedomsex/components/entity-date-update/commit/ef2e52eeb283a5cbc3a6af088ef3aa7d0e4b8d5e))

### [0.1.4](https://gitlab.com/freedomsex/entity-date-update/compare/0.1.3...0.1.4) (2021-12-17)


### Bug Fixes

* rename opt ([e3e935f](https://gitlab.com/freedomsex/entity-date-update/commit/e3e935f2fad19a450e44164698b1ee54184c94f0))

### [0.1.3](https://gitlab.com/freedomsex/entity-date-update/compare/0.1.2...0.1.3) (2021-12-17)


### Bug Fixes

* req ([a67b161](https://gitlab.com/freedomsex/entity-date-update/commit/a67b1618eb5ad80bc2160a3d6e3ed46083fe3014))

### [0.1.2](https://gitlab.com/freedomsex/entity-date-update/compare/0.1.1...0.1.2) (2021-12-17)


### Bug Fixes

* add ci ([01eb8c4](https://gitlab.com/freedomsex/entity-date-update/commit/01eb8c4023506dd3ad990f022dd17299f429153e))

### [0.1.1](https://gitlab.com/freedomsex/entity-date-update/compare/0.1.0...0.1.1) (2021-12-17)


### Bug Fixes

* cache ([f67d3ad](https://gitlab.com/freedomsex/entity-date-update/commit/f67d3ad9fe111cd6713979b52afd2466d1dc01bb))

## 0.1.0 (2021-12-17)
