<?php

namespace FreedomSex\Tests\Services;

use FreedomSex\Services\EntityDateUpdater;
use FreedomSex\Tests\Entity;
use PHPUnit\Framework\TestCase;

class EntityDateUpdaterTest extends TestCase
{
    private EntityDateUpdater $object;
    private Entity $entity;

    protected function setUp(): void
    {
        parent::setUp();
        $this->object = new EntityDateUpdater();
        $this->entity = new Entity();
    }

    public function testSetFieldValue()
    {
        $this->object->setFieldValue($this->entity, 'added');
        self::assertNotNull($this->entity->getAdded());
    }

    public function testGetFields()
    {
        $fields = $this->object->getFields($this->entity);
        self::assertIsArray($fields);
        self::assertContains('added', $fields);
    }

    public function testUpdateFields()
    {
        $this->object = new EntityDateUpdater(true);
        $date = new \DateTime();
        $date->sub(new \DateInterval('P5D'));
        $this->entity->setUpdated($date);
        $this->object->updateFields($this->entity, true);
        self::assertNotNull($this->entity->getUpdated());
        self::assertEquals($date, $this->entity->getUpdated());
        $this->object->updateFields($this->entity, false);
        self::assertNotEquals($date, $this->entity->getUpdated());
    }

    public function testUpdateInitializedFields()
    {
        $this->object->updateFields($this->entity, true);
        self::assertNull($this->entity->getAdded());
        self::assertNotNull($this->entity->getAddedAt());
        self::assertNull($this->entity->getUpdated());
        self::assertNotNull($this->entity->getUpdatedAt());
        self::assertNull($this->entity->getCreated());
        self::assertNotNull($this->entity->getCreatedAt());
        self::assertNull($this->entity->getChanged());
        self::assertNotNull($this->entity->getChangedAt());
    }

    public function testUpdateFieldsLegacy()
    {
        $this->object = new EntityDateUpdater(true);
        $this->object->updateFields($this->entity, true);
        self::assertNotNull($this->entity->getAdded());
        self::assertNotNull($this->entity->getAddedAt());
        self::assertNotNull($this->entity->getUpdated());
        self::assertNotNull($this->entity->getUpdatedAt());
        self::assertNotNull($this->entity->getCreated());
        self::assertNotNull($this->entity->getCreatedAt());
        self::assertNotNull($this->entity->getChanged());
        self::assertNotNull($this->entity->getChangedAt());
    }

    public function testIssetField()
    {
        self::assertTrue($this->object->issetField($this->entity, 'added'));
    }

    public function testSetDateTime()
    {
        $updated = new \DateTime('2000-01-01');
        $this->entity->setUpdated($updated);
        $this->object->setDateTime($this->entity, 'updated');
        self::assertEquals($updated, $this->entity->getUpdated());
        $this->object->setDateTime($this->entity, 'updated', true);
        self::assertNotEquals($updated, $this->entity->getUpdated());
    }

    public function testGetFieldValue()
    {
        $added = new \DateTime('2000-01-01');
        $this->entity->setAdded($added);
        self::assertNotNull($this->object->getFieldValue($this->entity, 'added'));

    }
}
