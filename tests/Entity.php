<?php

namespace FreedomSex\Tests;

class Entity
{
    private $added;
    private $addedAt;
    private $updated;
    private $updatedAt;
    private $created;
    private $createdAt;
    private $changed;
    private $changedAt;

    public function getAdded(): ?\DateTime
    {
        return $this->added;
    }

    public function setAdded(\DateTime $added): void
    {
        $this->added = $added;
    }

    public function getAddedAt(): ?\DateTime
    {
        return $this->addedAt;
    }

    public function setAddedAt(\DateTime $addedAt): void
    {
        $this->addedAt = $addedAt;
    }

    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getChanged()
    {
        return $this->changed;
    }

    public function setChanged($changed): void
    {
        $this->changed = $changed;
    }

    public function getChangedAt()
    {
        return $this->changedAt;
    }

    public function setChangedAt($changedAt): void
    {
        $this->changedAt = $changedAt;
    }


}