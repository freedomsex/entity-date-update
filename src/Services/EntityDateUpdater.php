<?php


namespace FreedomSex\Services;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;

class EntityDateUpdater
{
    const UPDATE_FIELDS = [
        'addedAt' => false,
        'createdAt' => false,
        'updatedAt' => true,
        'changedAt' => true,
    ];
    const UPDATE_FIELDS_LEGACY = [
        'added' => false,
        'created' => false,
        'updated' => true,
        'changed' => true,
    ];

    private PropertyInfoExtractor $propertyInfo;
    private PropertyAccessor $propertyAccessor;

    private bool $isLegacyFieldNames;

    public function __construct($isLegacyFieldNames = false)
    {
        $reflectionExtractor = new ReflectionExtractor();
        $this->propertyInfo = new PropertyInfoExtractor([$reflectionExtractor]);
        $this->propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->disableExceptionOnInvalidPropertyPath()
            ->getPropertyAccessor();
        $this->isLegacyFieldNames = $isLegacyFieldNames;
    }
    public function getFields($entity): ?array
    {
        return $this->propertyInfo->getProperties(get_class($entity));
    }

    public function issetField($entity, string $fieldName): bool
    {
        $fields = $this->getFields($entity);
        return in_array($fieldName, $fields);
    }

    public function getFieldValue($entity, string $fieldName)
    {
        return $this->propertyAccessor->getValue($entity, $fieldName);
    }

    public function setFieldValue($entity, string $fieldName)
    {
        $value = new \DateTime();
        $this->propertyAccessor->setValue($entity, $fieldName, $value);
    }

    public function setDateTime($entity, $fieldName, $forceUpdate = false)
    {
        $initialValue = $this->getFieldValue($entity, $fieldName);
        if ($this->issetField($entity, $fieldName) and (!$initialValue or $forceUpdate)) {
            $this->setFieldValue($entity, $fieldName);
        }
    }

    public function handleEntity($entity, array $fieldSet, $skipOverwriteInitial)
    {
        foreach ($fieldSet as $fieldName => $forceUpdate) {
            if ($forceUpdate and $skipOverwriteInitial) {
                $forceUpdate = false;
            }
            $this->setDateTime($entity, $fieldName, $forceUpdate);
        }
    }

    public function updateFields($entity, $skipOverwriteInitial)
    {
        $fieldSet = self::UPDATE_FIELDS;
        if ($this->isLegacyFieldNames) {
            $fieldSet = array_merge(self::UPDATE_FIELDS_LEGACY, $fieldSet);
        }
        $this->handleEntity($entity, $fieldSet, $skipOverwriteInitial);
    }

}
