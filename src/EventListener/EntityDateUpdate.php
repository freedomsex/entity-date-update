<?php


namespace FreedomSex\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use FreedomSex\Services\EntityDateUpdater;

class EntityDateUpdate implements EventSubscriber
{
    private EntityDateUpdater $entityDateUpdater;

    public function __construct(EntityDateUpdater $entityDateUpdater)
    {
        $this->entityDateUpdater = $entityDateUpdater;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->index($args, false);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->index($args, true);
    }

    public function index(LifecycleEventArgs $args, $skipOverwriteInitial)
    {
        $entity = $args->getObject();
        if (!$entity) {
            return;
        }
        $this->entityDateUpdater->updateFields($entity, $skipOverwriteInitial);
    }

}
